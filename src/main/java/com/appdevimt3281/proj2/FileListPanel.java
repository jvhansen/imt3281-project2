package com.appdevimt3281.proj2;

import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.DefaultListModel;
import javax.swing.GroupLayout;
import javax.swing.JButton;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JSeparator;
import javax.swing.ListSelectionModel;
import javax.swing.SwingConstants;

import java.awt.BorderLayout;

import java.awt.Dimension;
import java.awt.LayoutManager;

@SuppressWarnings("serial")
public class FileListPanel extends JPanel {

    public JList<String> fileNameList;
    public DefaultListModel fileListModel;
    public String[] selectedFiles;

    JButton removeButton;

    int counter;

    public FileListPanel() {
        setLayout(new BorderLayout());
        fileListModel = new DefaultListModel<String>();

        fileNameList = new JList<String>(fileListModel);
        fileNameList.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
        fileNameList.setSelectedIndex(0);
        fileNameList.setVisibleRowCount(-1);

        JScrollPane pane = new JScrollPane(fileNameList);
        pane.setSize(new Dimension(200, 100));

        removeButton = new JButton("Remove File");

        JPanel buttonPane = new JPanel();
        buttonPane.setLayout((LayoutManager) new BoxLayout(buttonPane, BoxLayout.LINE_AXIS));
        buttonPane.add(removeButton);
        removeButton.setEnabled(false);
        buttonPane.add(Box.createHorizontalStrut(5));
        buttonPane.add(new JSeparator(SwingConstants.VERTICAL));
        buttonPane.add(Box.createHorizontalStrut(5));
        buttonPane.setBorder(BorderFactory.createEmptyBorder(2, 2, 2, 2));
        buttonPane.setMaximumSize(new Dimension(100, 30));

        GroupLayout layout = new GroupLayout(this);
        setLayout(layout);

        layout.setAutoCreateGaps(true);
        layout.setAutoCreateContainerGaps(true);

        layout.setHorizontalGroup(
                layout.createParallelGroup(GroupLayout.Alignment.LEADING).addComponent(pane).addComponent(buttonPane));

        layout.setVerticalGroup(layout.createSequentialGroup().addComponent(pane).addComponent(buttonPane));

        setMaximumSize(new Dimension(200, 200));
    }

    public void addFileToModel(String fileName) {
        fileListModel.addElement(fileName);
    }
}
