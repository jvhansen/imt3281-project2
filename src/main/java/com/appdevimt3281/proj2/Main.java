package com.appdevimt3281.proj2;

import javax.swing.UIManager;

public class Main {
	public static void main(String[] args) {
		try {
			// Set cross-platform Java L&F (also called "Metal")
			UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
		} catch (Exception e) {
			// Do nothing, standard UI is used
		}
		new UI();
	}
}
