package com.appdevimt3281.proj2.Processing;

import java.util.List;

public class SentenceLevel {
    private String sentence;
    private List<String> sentenceAspects;
    private String sentencePolarity;

    public SentenceLevel(String sentence, List<String> sentenceAspect, String sentencePolarity) {
        this.sentence = sentence;
        this.sentencePolarity = sentencePolarity;
        this.sentenceAspects = sentenceAspect;
    }

    public String getSentence() {
        return sentence;
    }

    public String getSentencePolarity() {
        return sentencePolarity;
    }

    public List<String> getSentenceAspects() {
        return sentenceAspects;
    }

    public String getSentenceAspectsString() {
        String aspectString = "";
        for (String element : sentenceAspects) {
            aspectString += element + ", ";
        }
        return aspectString;
    }
}
