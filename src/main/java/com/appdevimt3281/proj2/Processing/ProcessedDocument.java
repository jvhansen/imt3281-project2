package com.appdevimt3281.proj2.Processing;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.List;

public class ProcessedDocument {
    private String fileName;
    private String filePolarity;
    private int totPos;
    private int totNeu;
    private int totNeg;
    List<SentenceLevel> sentences = new ArrayList<SentenceLevel>();

    public ProcessedDocument(DocumentBuilder builder) {
        this.fileName = builder.fileName; 
        this.filePolarity = builder.filePolarity;
        this.totPos = builder.totPos; 
        this.totNeu = builder.totNeu; 
        this.totNeg = builder.totNeg;
        this.sentences = builder.sentences; 
    }

    public String getFileName() {
        return fileName;
    }

    public String getPolarity(){
        return filePolarity;
    }

    public int getTotPos() {
        return totPos;
    }

    public int getTotNeu() {
        return totNeu;
    }

    public int getTotNeg() {
        return totNeg;
    }

    public List<SentenceLevel> getSentence(){
        return sentences;
    }

    public String getAspect() {
        // Create list of all aspects in document
        List<String> aspectList = new ArrayList<String>();

        // Go through each sentence in the document
        for (SentenceLevel sentence : sentences) {
            // Go through each aspect in that sentence
            for (String aspect : sentence.getSentenceAspects()) {
                // Add aspects to overview
                aspectList.add(aspect);
            }
        }
        
        // Create map of occurences of each word
        LinkedHashMap<String, Integer> aspectOccurences = new LinkedHashMap<String, Integer>();

        ArrayList<String> keysForMaxValue = new ArrayList<String>();
        int maxValue = 0;
        for (String aspect : aspectList) {
            int occurences = Collections.frequency(aspectList, aspect);
            aspectOccurences.put(aspect, occurences); 
            
            if (occurences == maxValue) {
                keysForMaxValue.add(aspect);
            }
            else if (occurences > maxValue) {
                keysForMaxValue = new ArrayList<String>();
                keysForMaxValue.add(aspect);
                maxValue = occurences;
            }
        }

        HashSet<String> uniqueList = new HashSet(keysForMaxValue);

        String aspects = "";
        for (String aspect : uniqueList) {
            aspects += aspect + " ";
        }
        return aspects;
    }
}
