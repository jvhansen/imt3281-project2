package com.appdevimt3281.proj2.Processing;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Properties;
import java.util.List;
import edu.stanford.nlp.pipeline.CoreDocument;
import edu.stanford.nlp.pipeline.CoreSentence;
import edu.stanford.nlp.pipeline.StanfordCoreNLP;

public class Processer {
  private static String[] nouns = { "NN", "NNS", "NNP", "NNPS" };

  public List<SentenceLevel> processSentences(String text) {
    return sentenceProcessing(pipeline(text));
  }

  public List<ProcessedDocument> processDocuments(HashMap<String, String> documents) {
    List<ProcessedDocument> processedDocuments = new ArrayList<ProcessedDocument>();

    for (String fileName : documents.keySet()) {
      // Process documents with CoreNLP
      List<SentenceLevel> processedSentences = sentenceProcessing(pipeline(documents.get(fileName)));

      // Instantiate variables to store stuff in
      int pos = 0;
      int neu = 0;
      int neg = 0;
      List<String> subjects = new ArrayList<String>();

      for (SentenceLevel sentence : processedSentences) {
        // Add subjects from processedText to overall document summarisation
        for (String subject : sentence.getSentenceAspects()) {
          subjects.add(subject);
        }

        // Summarise polarity
        String textPolarity = sentence.getSentencePolarity();
        switch (textPolarity) {
          case "Negative":
            neg++;
            break;
          case "Neutral":
            neu++;
            break;
          case "Positive":
            pos++;
            break;
          default:
            break;
        }
      }

      // "Calculate" overall polarity
      String polarity = "";
      if (pos > neu && pos > neg)
        polarity = "Positive";
      if (neg > pos && neg > neu)
        polarity = "Negative";
      if (neu > pos && neu > neg)
        polarity = "Neutral";

      // Add processed document (filename, polarity, total pos/neu/neg, processed sentences)
      processedDocuments.add(new DocumentBuilder(fileName).withPolarity(polarity).withtotPos(pos).withtotNeu(neu)
          .withtotNeg(neg).withSentenceAnalysis(processedSentences).build());
    }

    return processedDocuments;
  }

  private static CoreDocument pipeline(String text) {
    // Set up pipeline properties
    Properties props = new Properties();
    // Set the list of annotators to run
    props.setProperty("annotators", "tokenize,ssplit,pos,lemma,parse,sentiment");
    // Set a property for an annotator, in this case the coref annotator is being
    // set to use the neural algorithm
    props.setProperty("coref.algorithm", "neural");
    // Build pipeline
    StanfordCoreNLP pipeline = new StanfordCoreNLP(props);
    // Create a document object
    CoreDocument document = new CoreDocument(text);
    // Annnotate the document
    pipeline.annotate(document);

    return document;
  }

  private List<SentenceLevel> sentenceProcessing(CoreDocument document) {

    // Create structure to hold information on finished senctence
    List<SentenceLevel> processedSentences = new ArrayList<SentenceLevel>();

    for (CoreSentence sentence : document.sentences()) {
      // Instantiate a list to hold all subjects
      List<String> subjects = new ArrayList<String>();

      // Goes through all types of nouns and links it to the polarity of the sentence
      for (String noun : nouns) {
        int nounIndex = sentence.posTags().indexOf(noun);

        if (nounIndex >= 0) {
          // Gets the word/subject in tokenized form & add to list
          String subject = sentence.tokens().get(nounIndex).lemma();
          subjects.add(subject);
        }
      }

      // Add ProcessedText element to list - with sentence text, subject list and
      // polarity
      processedSentences.add(new SentenceLevel(sentence.text(), subjects, sentence.sentiment()));
    }

    return processedSentences;
  }

}
