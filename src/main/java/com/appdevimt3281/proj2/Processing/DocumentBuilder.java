package com.appdevimt3281.proj2.Processing;

import java.util.ArrayList;
import java.util.List;

public class DocumentBuilder {
    String fileName;
    String filePolarity;
    int totPos;
    int totNeu;
    int totNeg;
    List<SentenceLevel> sentences = new ArrayList<SentenceLevel>();

    public DocumentBuilder(String name) {
        this.fileName = name;
    }

    public DocumentBuilder withPolarity(String polarity) {
        this.filePolarity = polarity;
        return this;
    }

    public DocumentBuilder withSentenceAnalysis(List<SentenceLevel> sentences) {
        this.sentences = sentences;
        return this;
    }

    public DocumentBuilder withtotPos(int sentimentCount) {
        this.totPos = sentimentCount;
        return this;
    }

    public DocumentBuilder withtotNeu(int sentimentCount) {
        this.totNeu = sentimentCount;
        return this;
    }

    public DocumentBuilder withtotNeg(int sentimentCount) {
        this.totNeg = sentimentCount;
        return this;
    }

    public ProcessedDocument build() {
        return new ProcessedDocument(this);
    }
}