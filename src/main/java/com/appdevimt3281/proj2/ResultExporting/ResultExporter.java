package com.appdevimt3281.proj2.ResultExporting;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.filechooser.FileFilter;
import javax.swing.filechooser.FileNameExtensionFilter;
import java.util.Iterator;

import com.appdevimt3281.proj2.Processing.SentenceLevel;
import com.opencsv.CSVWriter;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.*;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import java.io.File;
import java.io.FileWriter;
import java.util.List;
import com.appdevimt3281.proj2.Processing.ProcessedDocument;

public class ResultExporter {
    public ResultExporter(List<ProcessedDocument> list) {
        JFrame selection = new JFrame();
        JFileChooser chooser = new JFileChooser();
        FileFilter csvType = new FileNameExtensionFilter("CSV File (.csv)", "csv");
        FileFilter xmlType = new FileNameExtensionFilter("XML File (.xml)", "XML");

        chooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
        chooser.setDialogTitle("Export to..");
        chooser.setDialogType(JFileChooser.SAVE_DIALOG);

        chooser.addChoosableFileFilter(csvType);
        chooser.addChoosableFileFilter(xmlType);
        chooser.setFileFilter(xmlType);
        int selectedOption = chooser.showSaveDialog(selection);
        if (selectedOption == JFileChooser.APPROVE_OPTION) {
            String saveloc = chooser.getSelectedFile().getAbsolutePath();
            // FileFilter for XML and CSV
            if (chooser.getFileFilter() == xmlType) {
                try {
                    exportXML(saveloc, list);
                } catch (ParserConfigurationException e) {
                    System.out.println("Error line 47");
                } catch (TransformerException e) {
                    System.out.println("Could not transform DOC to XML");
                }
            }
            if (chooser.getFileFilter() == csvType) {
                try {
                    exportCSV(saveloc, list);
                } catch (Exception e) {
                    System.out.println("Could not create CSV file.");
                }
            }
        }
    }

    private void exportCSV(String loc, List<ProcessedDocument> list) throws Exception {
        CSVWriter writer = new CSVWriter(new FileWriter(loc + "/" + "export.csv"));
        // For every document:
        Iterator<ProcessedDocument> arrayListIterator = list.iterator();
        while (arrayListIterator.hasNext()) {
            ProcessedDocument currentDocument = arrayListIterator.next();
            // For every sentence in the document:
            Iterator<SentenceLevel> sentenceListIterator = currentDocument.getSentence().iterator();
            while (sentenceListIterator.hasNext()) {
                SentenceLevel currentSentenceLevel = sentenceListIterator.next();
                // Write to CSVFile as {Sentence, Aspect, Polarity}
                String[] toCSV = { currentSentenceLevel.getSentence(),
                        currentSentenceLevel.getSentenceAspects().toString(),
                        currentSentenceLevel.getSentencePolarity() };
                writer.writeNext(toCSV);
            }
        }
        writer.close();
    }

    private void exportXML(String loc, List<ProcessedDocument> list)
            throws ParserConfigurationException, TransformerException {
        // Format:
        // <Sentence + ID>
        // <Comment></Comment>
        // <Aspect></Aspect>
        // <Polarity></Polarity>
        // </Sentence>

        // Sets up the DocumentBuilder for XML
        String XMLroot = "Sentences";
        String id = "Sentence";
        String XMLComment = "Comment";
        String XMLAspect = "Aspect";
        String XMLPolarity = "Polarity";
        int sentenceNumber = 0;

        DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
        DocumentBuilder db = dbf.newDocumentBuilder();

        // Creating our XML-root:
        Document doc = db.newDocument();
        Element rootElement = doc.createElement(XMLroot);
        doc.appendChild(rootElement);

        // For every document:
        Iterator<ProcessedDocument> arrayListIterator = list.iterator();
        while (arrayListIterator.hasNext()) {
            ProcessedDocument currentDocument = arrayListIterator.next();

            // For every sentence:
            Iterator<SentenceLevel> sentenceListIterator = currentDocument.getSentence().iterator();
            while (sentenceListIterator.hasNext()) {
                sentenceNumber += 1;

                // Creating our elements
                Element idElement = doc.createElement(id + sentenceNumber);
                rootElement.appendChild(idElement);

                Element Comment = doc.createElement(XMLComment);
                idElement.appendChild(Comment);

                Element Aspect = doc.createElement(XMLAspect);
                idElement.appendChild(Aspect);

                Element Polarity = doc.createElement(XMLPolarity);
                idElement.appendChild(Polarity);

                // Add values to each node.
                SentenceLevel currentSentenceLevel = sentenceListIterator.next();
                Comment.appendChild(doc.createTextNode(currentSentenceLevel.getSentence()));
                Aspect.appendChild(doc.createTextNode(currentSentenceLevel.getSentenceAspects().toString()));
                Polarity.appendChild(doc.createTextNode(currentSentenceLevel.getSentencePolarity()));

                // Transform DocumentBuilder to XML File:
                TransformerFactory tf = TransformerFactory.newDefaultInstance();
                Transformer t = tf.newTransformer();
                DOMSource src = new DOMSource(doc);
                StreamResult result = new StreamResult(new File(loc + "/" + "export.xml"));
                t.transform(src, result);
            }
        }
    }
}
