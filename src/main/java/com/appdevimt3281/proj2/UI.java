package com.appdevimt3281.proj2;

import java.awt.Dimension;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

import javax.swing.BorderFactory;
import javax.swing.BoxLayout;
import javax.swing.GroupLayout;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenuBar;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;

import javax.swing.JTabbedPane;
import javax.swing.JTextArea;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.table.DefaultTableModel;

import com.appdevimt3281.proj2.FileReading.FileReading;
import com.appdevimt3281.proj2.Processing.ProcessedDocument;
import com.appdevimt3281.proj2.Processing.Processer;
import com.appdevimt3281.proj2.Processing.SentenceLevel;
import com.appdevimt3281.proj2.ResultExporting.ResultExporter;

public class UI {
    HashMap<String, String> fileMap = new HashMap<String, String>();
    List<ProcessedDocument> processedDocuments = new ArrayList<ProcessedDocument>();

    JPanel mainPanel;
    JButton exportBtn;
    DefaultTableModel sentenceModel;
    DefaultTableModel documentModel;
    JTextArea sentenceLevelText;
    JTextArea documentLevelText;
    JTable sentenceTable;
    JTable documentTable;
    FileListPanel fileListPane;
    JTabbedPane tabbedPane;
    Processer processer;

    JLabel allFiles;
    JLabel totPos;
    JLabel totNeu;
    JLabel totNeg;

    String selectedFile;

    public UI() {
        // Create and set up the frame window
        JFrame.setDefaultLookAndFeelDecorated(true);
        JFrame mainFrame = new JFrame("Sentiment Analyser");
        mainFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        
        //Add menubar
        JMenuBar menuBar = new TopBar();

        sentenceModel = new DefaultTableModel();
        sentenceTable = new JTable(sentenceModel);
        sentenceModel.addColumn("Sentence");
        sentenceModel.addColumn("Aspect(s)");
        sentenceModel.addColumn("Polarity");
        JScrollPane scrollDetailed = new JScrollPane(sentenceTable);

        documentModel = new DefaultTableModel();
        documentTable = new JTable(documentModel);
        documentModel.addColumn("File name");
        documentModel.addColumn("Aspect(s)");
        documentModel.addColumn("Polarity");
        JScrollPane scrollSimple = new JScrollPane(documentTable);
        
        // Create select file button and it's action
        JButton btnSelectFiles = new JButton("Select files");
        btnSelectFiles.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                // Select files & return them here
                btnSelectFiles.setText("Loading...");
                FileReading reader = new FileReading();
                HashMap<String, String> addedFiles = reader.getFiles();
                addedFiles.forEach((key, value) -> fileMap.putIfAbsent(key, value));

                fileListPane.fileListModel.removeAllElements();
                for (String key : fileMap.keySet()) {
                    fileListPane.addFileToModel(key);
                }

                // Create file processer and update textView
                processer = new Processer();
                processedDocuments = processer.processDocuments(fileMap);
                updateDocumentLevel(processedDocuments);
                btnSelectFiles.setText("Select files");
            }
        });

        exportBtn = new JButton("Export");
        exportBtn.setEnabled(false);
        exportBtn.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                new ResultExporter(processedDocuments);
           }
       });

        // Labels for som statistics
        allFiles = new JLabel("All files");
        allFiles.setFont(new Font("Verdana", Font.BOLD, 12));
        totPos = new JLabel("Tot Positive Files: 0");
        totNeu = new JLabel("Tot Neutral Files: 0");
        totNeg = new JLabel("Tot Negative Files: 0");
        JPanel statisticPane = new JPanel();
        statisticPane.setLayout(new BoxLayout(statisticPane, BoxLayout.PAGE_AXIS));
        statisticPane.add(allFiles);
        statisticPane.add(totPos);
        statisticPane.add(totNeu);
        statisticPane.add(totNeg);
        statisticPane.setBorder(BorderFactory.createEmptyBorder(2, 2, 2, 2));

        // Pane with list of files, with listner for list clicks
        fileListPane = new FileListPanel();
        fileListPane.setOpaque(true);
        fileListPane.setMaximumSize(new Dimension(50, 200));
        fileListPane.fileNameList.addListSelectionListener(new ListSelectionListener() {
            @Override
            public void valueChanged(ListSelectionEvent e) {
                if (!e.getValueIsAdjusting()) {
                    if (fileListPane.fileNameList.getSelectedIndex() == -1) {
                        // No selection, disable fire button.
                        fileListPane.removeButton.setEnabled(false);
                    } else {
                        // Selection, enable the fire button.
                        exportBtn.setEnabled(true);
                        selectedFile = fileListPane.fileNameList.getSelectedValue();
                        fileListPane.removeButton.setEnabled(true);
                        updateSentenceLevel(processedDocuments, selectedFile);

                    }
                }
            }
        });
        
        fileListPane.removeButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                int index = fileListPane.fileNameList.getSelectedIndex();
                var fileName = fileListPane.fileNameList.getSelectedValue();
                fileMap.remove(fileName);

                for (int i = 0; i < processedDocuments.size(); i++) {
                    if (processedDocuments.get(i).getFileName() == fileName) {
                        processedDocuments.remove(i);
                    }
                }
                updateDocumentLevel(processedDocuments);

                fileListPane.fileListModel.remove(index);

                for (int i = 0; i < documentModel.getRowCount(); i++) {
                    if (documentModel.getValueAt(i, 0).toString() == fileName) {
                        documentModel.removeRow(i);
                    }
                }
                documentTable.revalidate();
                documentTable.repaint();

                int size = fileListPane.fileListModel.getSize();

                if (size == 0) {
                    fileListPane.removeButton.setEnabled(false);
                } else {
                    if (index == size) {
                        index--;
                    }
                    fileListPane.fileNameList.setSelectedIndex(index);
                    fileListPane.fileNameList.ensureIndexIsVisible(index);
                }
            }
        });

        // textAreas in tabbed panes for output
        tabbedPane = new JTabbedPane();
        tabbedPane.add("Document", scrollSimple);
        tabbedPane.add("Sentence", scrollDetailed);
        tabbedPane.addChangeListener(new ChangeListener() {
            @Override
            public void stateChanged(ChangeEvent e) {
                if(scrollSimple == tabbedPane.getSelectedComponent()) {
                    updateDocumentLevel(processedDocuments);
                }
                else {
                    updateSentenceLevel(processedDocuments, selectedFile);
                }
            }
        });

        mainPanel = new JPanel();
        mainPanel.setSize(1000,1000);
        GroupLayout layout = new GroupLayout(mainPanel);
        layout.setAutoCreateGaps(true);
        layout.setAutoCreateContainerGaps(true);
        mainPanel.setLayout(layout);

        //Set frame layout
        layout.setHorizontalGroup(layout.createSequentialGroup()
        .addGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING)
        .addComponent(btnSelectFiles)
        .addComponent(fileListPane)
        .addComponent(statisticPane))
        .addComponent(exportBtn)
        .addComponent(tabbedPane));
        layout.setVerticalGroup(layout.createParallelGroup()
        .addGroup(layout.createSequentialGroup()
        .addGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING)
        .addComponent(btnSelectFiles)
        .addComponent(exportBtn))
        .addComponent(fileListPane)
        .addComponent(statisticPane))
        .addComponent(tabbedPane));
       
        mainFrame.add(mainPanel);
        mainFrame.setJMenuBar(menuBar);
        mainFrame.setPreferredSize(new Dimension(1000, 800));
        mainFrame.pack();
        mainFrame.setLocationRelativeTo(null);
        mainFrame.setVisible(true);
    }

    public void updateSentenceLevel(List<ProcessedDocument> list, String file){
        //String sentenceText = "";
        Iterator<ProcessedDocument> arrayListIterator = list.iterator();

        if (sentenceModel.getRowCount() > 0) {
            for (int i = sentenceModel.getRowCount() - 1; i > -1; i--) {
                sentenceModel.removeRow(i);
            }
        }

        while (arrayListIterator.hasNext()) {
            ProcessedDocument currentDocument = arrayListIterator.next();
            if (currentDocument.getFileName() == file){
                totPos.setText("Tot Positive: " + currentDocument.getTotPos());
                totNeu.setText("Tot Neutral: " + currentDocument.getTotNeu());
                totNeg.setText("Tot Negative: " + currentDocument.getTotNeg());
                allFiles.setText("Single File");

                Iterator<SentenceLevel> sentenceListIterator = currentDocument.getSentence().iterator();
                while (sentenceListIterator.hasNext()) {
                    SentenceLevel currentSentenceLevel = sentenceListIterator.next();
                    sentenceModel.addRow(new Object[]{currentSentenceLevel.getSentence(), currentSentenceLevel.getSentenceAspectsString(), currentSentenceLevel.getSentencePolarity()});
                }
            }   
        }
    }

    public void updateDocumentLevel(List<ProcessedDocument> list) {
        int pos = 0;
        int neu = 0;
        int neg = 0;

        Iterator<ProcessedDocument> arrayListIterator = list.iterator();

        while (arrayListIterator.hasNext()) {
            ProcessedDocument currentDocument = arrayListIterator.next();
            String currentPolarity = currentDocument.getPolarity();

            var isNewDocument = true;

            for (int i = 0; i < documentModel.getRowCount(); i++) {
                if (documentModel.getValueAt(i, 0) == currentDocument.getFileName())
                {
                    isNewDocument = false;
                    break;
                }
            }

            if (isNewDocument) {
                documentModel.addRow(new Object[]{currentDocument.getFileName(), currentDocument.getAspect(), currentPolarity});
            }

            switch (currentPolarity) {
                case "Positive":
                    pos++;
                    break;
                case "Neutral":
                    neu++;
                    break;
                case "Negative":
                    neg++;
                    break;
                default:
                    break;
            }
        }

        allFiles.setText("All files");
        totPos.setText("Tot Positive Files: " + pos);
        totNeu.setText("Tot Neutral Files: " + neu);
        totNeg.setText("Tot Negative Files: " + neg);
    }
}