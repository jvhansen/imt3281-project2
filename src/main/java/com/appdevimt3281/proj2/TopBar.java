package com.appdevimt3281.proj2;

import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JFrame;
import javax.swing.JOptionPane;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

@SuppressWarnings("serial")
public class TopBar extends JMenuBar {

    JMenu options;
    JMenuItem howTo;
    JMenuItem about;
    private String howToText = "How to:\nTo upload a file, simply press the Select files button.\nDepending on the file size it may take some time to\nprocess the document.\nTo show all the file analytics, choose the Document tab,\nand for one single file, pick one in the list and press the Sentence tab.\nExport the precessed data with the Export button";
    private String aboutText = "About:\nThis is a sentiment analyzer application which takes a text document( .txt, .doc, .docx, .csv),\nprocesses the text and displayes it.\nThere are statistics shown in the labels under where the files are diplayed\nand the processed data in the two tabs.";

    public TopBar() {
        options = new JMenu("Options");
        howTo = new JMenuItem("How to");
        about = new JMenuItem("About");

        options.add(howTo);
        options.add(about);
        add(options);

        howTo.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                showDialog(howToText);
            }
        });

        about.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                showDialog(aboutText);
            }
        });
    }

    public void showDialog(String content) {
        JFrame dialogFrame = new JFrame();
        JOptionPane.showMessageDialog(dialogFrame, content);
    }
    
}
