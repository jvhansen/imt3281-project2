package com.appdevimt3281.proj2.FileReading;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileReader;
import java.io.IOException;
import java.nio.file.Files;
import java.util.HashMap;
import java.util.List;

import javax.swing.JFileChooser;
import javax.swing.JFrame;

import com.opencsv.CSVReader;

import org.apache.poi.xwpf.extractor.XWPFWordExtractor;
import org.apache.poi.xwpf.usermodel.XWPFDocument;

import org.apache.poi.hwpf.HWPFDocument;
import org.apache.poi.hwpf.extractor.WordExtractor;

public class FileReading {
    HashMap<String, String> fileMap;

    public FileReading() {
        // Creates file selection window
        JFileChooser chooser = new JFileChooser();
        chooser.addChoosableFileFilter(new FileTypeFilter());
        chooser.setAcceptAllFileFilterUsed(false);
        chooser.setMultiSelectionEnabled(true);
        chooser.showOpenDialog(new JFrame());

        File[] files = chooser.getSelectedFiles();

        // Add file strings to list
        fileMap = new HashMap<String, String>();
        for (File file : files) {
            if (file.getName().toLowerCase().endsWith(".txt")) {
                fileMap.put(file.getName(), txtReader(file));
            } else if (file.getName().toLowerCase().endsWith(".docx")) {
                fileMap.put(file.getName(), docxReader(file));
            } else if (file.getName().toLowerCase().endsWith(".doc")) {
                fileMap.put(file.getName(), docReader(file));
            } else if (file.getName().toLowerCase().endsWith(".csv")) {
                fileMap.put(file.getName(), csvReader(file));
            }
        }
    }

    public HashMap<String, String> getFiles() {
        return fileMap;
    }

    private String txtReader(File file) {
        try {
            return Files.readString(file.toPath());
        } catch (IOException e) {
            System.out.println("Cannot open specified file: " + file.getName());
            return null;
        }
    }

    private String docReader(File file) {
        try {
            FileInputStream fis = new FileInputStream(file);
            XWPFDocument document = new XWPFDocument(fis);
            XWPFWordExtractor extractor = new XWPFWordExtractor(document);
            String text = extractor.getText();
            extractor.close();

            return text;

        } catch (Exception e) {
            System.out.println("Cannot read specified file: " + file.getName());
            return null;
        }
    }

    private String docxReader(File file) {
        try {
            FileInputStream fis = new FileInputStream(file);
            HWPFDocument document = new HWPFDocument(fis);
            WordExtractor extractor = new WordExtractor(document);

            String text = extractor.getText();
            extractor.close();

            return text;

        } catch (IllegalArgumentException e) {
            // Sometimes .docx are really .doc formatted
            return docReader(file);
        } catch (Exception e) {
            System.out.println("Cannot read specified file: " + file.getName());
            return null;
        }
    }

    private String csvReader(File file) {
        try {
            CSVReader reader = new CSVReader(new FileReader(file));
            List<String[]> entries = reader.readAll();

            StringBuilder builder = new StringBuilder();
            // Loop through each
            for (String[] entry : entries) {
                String newLine = "";
                for (String property : entry) {
                    newLine += " " + property;
                }
                builder.append(newLine + "\n");
            }

            return builder.toString();

        } catch (Exception e) {
            System.out.println("Cannot read specified file: " + file.getName());
            return null;
        }
    }
}
