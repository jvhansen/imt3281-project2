package com.appdevimt3281.proj2.FileReading;

import java.io.File;

import javax.swing.filechooser.FileFilter;

public class FileTypeFilter extends FileFilter {
   public final static String TXT = "txt";
   public final static String DOC = "doc";
   public final static String DOCX = "docx";
   public final static String CSV = "csv";

   @Override
   public boolean accept(File f) {
      if (f.isDirectory()) {
         return true;
      }

      String extension = getExtension(f);
      if (extension != null) {
         if (extension.equals(TXT) || extension.equals(DOC) || extension.equals(DOCX) || extension.equals(CSV)) {
            return true;
         } else {
            return false;
         }
      }
      return false;
   }

   @Override
   public String getDescription() {
      return ".txt, .doc, .docx or .csv only";
   }

   String getExtension(File f) {
      String ext = null;
      String s = f.getName();
      int i = s.lastIndexOf('.');

      if (i > 0 && i < s.length() - 1) {
         ext = s.substring(i + 1).toLowerCase();
      }
      return ext;
   }

}
