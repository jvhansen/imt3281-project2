# Project 2 - IMT3281, Autumn 2020 #

### Participants ###

* Sindre Opskar Hjelle
* June V. E. Hansen
* Emil J. T. Hegdal
* Ole K. L. Lyso

### Usage ###

See instructions in menu bar after launching the program

### UML diagram ###

![picture](proj2-figure.png)
